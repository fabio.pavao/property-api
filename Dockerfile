FROM openjdk:8-jdk

COPY /build/libs/property-api-*.jar app.jar

CMD ["java", "-jar", "app.jar"]