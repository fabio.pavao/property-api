package br.com.fpavao.property.api.repository;

import br.com.fpavao.property.api.model.Province;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Set;

/**
 *
 * <p>Interface responsável pelas operações de banco de dados
 * específicas para a entidade {@link Province}.</p>
 *
 * <p>Estende a interface padrão {@link MongoRepository} do Spring Data,
 * que expõem recursos de persistência e operações de CRUD,
 * ajudando o desenvolvedor com as operações mais comuns.</p>
 *
 * @author Fabio E. Pavão
 * @since 1.0 - 12/06/17
 */
public interface ProvinceRepository extends MongoRepository<Province, String> {

	@Query("{ $and : [ { bottomRightX: { $gte: ?0 } }, { upperLeftX: { $lte: ?0 } }, { upperLeftY: { $gte: ?1 } }, { bottomRightY: { $lte: ?1 } } ] }")
	Set<Province> findByLocation(int x, int y);
}
