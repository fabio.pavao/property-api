package br.com.fpavao.property.api.service;

import br.com.fpavao.property.api.repository.ProvinceRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Fabio E. Pavão
 * @since 1.0 - 12/06/17
 */
public class PropertyLocation {
	
	private ProvinceRepository provinceRepository;
	
	@Autowired
	public PropertyLocation(ProvinceRepository provinceRepository) {
		this.provinceRepository = provinceRepository;
	}
	
	public void getProvince(int x, int y) {
		
	}
	
	
	boolean isPInR(double px, double py, double r1x, double r1y, double r2x, double r2y) {
		
		if(px > r1x && px < r2x && py < r1y && py > r2y){
			//It is inside
			return true;
		}
		return false;
	}
	
}
