package br.com.fpavao.property.api;

import br.com.fpavao.property.api.dto.PropertiesDTO;
import br.com.fpavao.property.api.model.Province;
import br.com.fpavao.property.api.repository.PropertyRepository;
import br.com.fpavao.property.api.repository.ProvinceRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import cz.jirutka.spring.embedmongo.EmbeddedMongoFactoryBean;
import java.io.IOException;
import java.io.InputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {
	
	static final Logger LOG = LoggerFactory.getLogger(Application.class);
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}
	
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
					.allowedOrigins("*")
					.allowedHeaders("*")
					.allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
					.allowCredentials(true)
					.maxAge(3600);
			}
			
		};
	}

	private static final String MONGO_DB_URL = "localhost";
	private static final String MONGO_DB_NAME = "embeded_db";

	@Bean
	public MongoTemplate mongoTemplate() throws IOException {
		EmbeddedMongoFactoryBean mongo = new EmbeddedMongoFactoryBean();
		mongo.setBindIp(MONGO_DB_URL);
		MongoClient mongoClient = mongo.getObject();
		MongoTemplate mongoTemplate = new MongoTemplate(mongoClient, MONGO_DB_NAME);
		return mongoTemplate;
	}

	/*
	Inicialização da base de dados com províncias.
	 */
	@Bean
	public CommandLineRunner loadProvinces(ProvinceRepository repository) {
		
		return (args) -> {
			repository.save(new Province("Gode", 0, 1000, 600, 500));
			repository.save(new Province("Ruja", 400, 1000, 1100, 500));
			repository.save(new Province("Jaby", 1100, 1000, 1400, 500));
			repository.save(new Province("Scavy", 0, 500, 600, 0));
			repository.save(new Province("Groola", 600, 500, 800, 0));
			repository.save(new Province("Nova", 800, 500, 1400, 0));
			
			LOG.info("Provincias carregadas com sucesso.");
		};
	}
	
	/*
	Inicialização da base de dados com propriedades a partir do json definido.
	 */
	@Bean
	public CommandLineRunner loadProperties(PropertyRepository propertyRepository,
	                                        ProvinceRepository provinceRepository) {
		
		return (args) -> {
			ResourceLoader resourceLoader = new DefaultResourceLoader();
			InputStream is = resourceLoader.getResource("classpath:properties.json").getInputStream();
			PropertiesDTO propertiesDTO = new ObjectMapper().readValue(is, PropertiesDTO.class);

			propertiesDTO
					.getProperties()
					.forEach(property -> {
						provinceRepository
								.findByLocation(property.getX(), property.getY())
								.forEach(province -> property.getProvinces().add(province.getName()));
						propertyRepository.save(property);
					});

			LOG.info("Propriedades carregadas com sucesso.");
		};
	}
}