package br.com.fpavao.property.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;

/**
 *
 * <p>Classe que representa uma propriedade em Spotippos.</p>
 *
 * @author Fabio E. Pavão
 * @since 1.0 - 10/06/17
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Property {
	
	@NotNull @Id
	private Long id;
	
	@NotEmpty
	private String title;
	
	@NotNull
	private Integer price;
	
	@NotEmpty
	private String description;
	
	@NotNull @Min(0) @Max(1400)
	private Integer x;
	
	@NotNull @Min(0) @Max(1000)
	private Integer y;
	
	@NotNull @Min(1) @Max(5)
	private Integer beds;
	
	@NotNull @Min(1) @Max(4)
	private Integer baths;
	
	@NotNull @Min(20) @Max(240)
	private Integer squareMeters;
	
	private Set<String> provinces;
	
	public Property() { }
	
	public Property(Long id, String title, Integer price, String description, Integer x,
	                Integer y, Integer beds, Integer baths, Integer squareMeters) {
		this.id = id;
		this.title = title;
		this.price = price;
		this.description = description;
		this.x = x;
		this.y = y;
		this.beds = beds;
		this.baths = baths;
		this.squareMeters = squareMeters;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Integer getPrice() {
		return price;
	}
	
	public void setPrice(Integer price) {
		this.price = price;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getX() {
		return x;
	}
	
	public void setX(Integer x) {
		this.x = x;
	}
	
	public Integer getY() {
		return y;
	}
	
	public void setY(Integer y) {
		this.y = y;
	}
	
	public Integer getBeds() {
		return beds;
	}
	
	public void setBeds(Integer beds) {
		this.beds = beds;
	}
	
	public Integer getBaths() {
		return baths;
	}
	
	public void setBaths(Integer baths) {
		this.baths = baths;
	}
	
	public Integer getSquareMeters() {
		return squareMeters;
	}
	
	public void setSquareMeters(Integer squareMeters) {
		this.squareMeters = squareMeters;
	}
	
	public Set<String> getProvinces() {
		
		if (provinces == null)
			provinces = new LinkedHashSet<>();
		
		return provinces;
	}
	
	public void setProvinces(Set<String> provinces) {
		this.provinces = provinces;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Property property = (Property) o;
		return id.equals(property.id);
	}
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Property{");
		sb.append("id=").append(id);
		sb.append(", title='").append(title).append('\'');
		sb.append(", price=").append(price);
		sb.append(", description='").append(description).append('\'');
		sb.append(", x=").append(x);
		sb.append(", y=").append(y);
		sb.append(", beds=").append(beds);
		sb.append(", baths=").append(baths);
		sb.append(", squareMeters=").append(squareMeters);
		sb.append(", provinces=").append(provinces);
		sb.append('}');
		return sb.toString();
	}
}
