package br.com.fpavao.property.api.model;

import org.springframework.data.annotation.Id;

/**
 *
 * <p>Classe que representa uma província de Spotippos.</p>
 *
 * @author Fabio E. Pavão
 * @since 1.0 - 11/06/17
 */
public class Province {
	
	@Id
	private String name;
	
	private int upperLeftX;
	private int upperLeftY;
	private int bottomRightX;
	private int bottomRightY;
	
	public Province() { }
	
	public Province(String name, int upperLeftX, int upperLeftY,
	                int bottomRightX, int bottomRightY) {
		this.name = name;
		this.upperLeftX = upperLeftX;
		this.upperLeftY = upperLeftY;
		this.bottomRightX = bottomRightX;
		this.bottomRightY = bottomRightY;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getUpperLeftX() {
		return upperLeftX;
	}
	
	public void setUpperLeftX(int upperLeftX) {
		this.upperLeftX = upperLeftX;
	}
	
	public int getUpperLeftY() {
		return upperLeftY;
	}
	
	public void setUpperLeftY(int upperLeftY) {
		this.upperLeftY = upperLeftY;
	}
	
	public int getBottomRightX() {
		return bottomRightX;
	}
	
	public void setBottomRightX(int bottomRightX) {
		this.bottomRightX = bottomRightX;
	}
	
	public int getBottomRightY() {
		return bottomRightY;
	}
	
	public void setBottomRightY(int bottomRightY) {
		this.bottomRightY = bottomRightY;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		Province province = (Province) o;
		
		if (upperLeftX != province.upperLeftX) return false;
		if (upperLeftY != province.upperLeftY) return false;
		if (bottomRightX != province.bottomRightX) return false;
		if (bottomRightY != province.bottomRightY) return false;
		return name.equals(province.name);
	}
	
	@Override
	public int hashCode() {
		int result = name.hashCode();
		result = 31 * result + upperLeftX;
		result = 31 * result + upperLeftY;
		result = 31 * result + bottomRightX;
		result = 31 * result + bottomRightY;
		return result;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Province{");
		sb.append("name='").append(name).append('\'');
		sb.append(", upperLeftX=").append(upperLeftX);
		sb.append(", upperLeftY=").append(upperLeftY);
		sb.append(", bottomRightX=").append(bottomRightX);
		sb.append(", bottomRightY=").append(bottomRightY);
		sb.append('}');
		return sb.toString();
	}
}
