package br.com.fpavao.property.api.dto;

import br.com.fpavao.property.api.model.Property;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Set;

/**
 *
 * Classe para transporte dos resultados da busca de imóveis em Spotippos.
 *
 * @author Fabio E. Pavão
 * @since 1.0 - 11/06/17
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PropertiesDTO {
	
	private int totalProperties;
	private Set<Property> properties;
	
	public PropertiesDTO() { }
	
	public PropertiesDTO(Set<Property> properties) {
		this.totalProperties = properties.size();
		this.properties = properties;
	}
	
	public int getTotalProperties() {
		return totalProperties;
	}
	
	public void setTotalProperties(int totalProperties) {
		this.totalProperties = totalProperties;
	}
	
	public Set<Property> getProperties() {
		return properties;
	}
	
	public void setProperties(Set<Property> properties) {
		this.properties = properties;
	}
}
