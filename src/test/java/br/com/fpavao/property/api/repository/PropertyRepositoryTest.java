package br.com.fpavao.property.api.repository;

import br.com.fpavao.property.api.DefaultConfigTest;
import br.com.fpavao.property.api.model.Property;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;


/**
 * @author Fabio E. Pavão
 * @since 1.0 - 11/06/17
 */
public class PropertyRepositoryTest extends DefaultConfigTest {
	
	@Autowired
	private PropertyRepository propertyRepository;
	
	@Test
	public void shouldReturnGodeProperties() {
		Set<Property> properties = propertyRepository.findByArea(0, 1000, 600, 500);
		assertThat(properties, hasSize(353));
	}
	
	@Test
	public void shouldReturnRujaProperties() {
		Set<Property> properties = propertyRepository.findByArea(400, 1000, 1100, 500);
		assertThat(properties, hasSize(1969));
	}
	
	@Test
	public void shouldReturnJabyProperties() {
		Set<Property> properties = propertyRepository.findByArea(1100, 1000, 1400, 500);
		assertThat(properties, hasSize(884));
	}
	
	@Test
	public void shouldReturnScavyProperties() {
		Set<Property> properties = propertyRepository.findByArea(0, 500, 600, 0);
		assertThat(properties, hasSize(2081));
	}
	
	@Test
	public void shouldReturnGroolaProperties() {
		Set<Property> properties = propertyRepository.findByArea(600, 500, 800, 0);
		assertThat(properties, hasSize(964));
	}
	
	@Test
	public void shouldReturnNovaProperties() {
		Set<Property> properties = propertyRepository.findByArea(800, 500, 1400, 0);
		assertThat(properties, hasSize(3363));
	}
}
