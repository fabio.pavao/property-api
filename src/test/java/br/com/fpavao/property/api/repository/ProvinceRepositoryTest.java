package br.com.fpavao.property.api.repository;

import br.com.fpavao.property.api.DefaultConfigTest;
import br.com.fpavao.property.api.model.Province;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * @author Fabio E. Pavão
 * @since 1.0 - 15/06/17
 */
public class ProvinceRepositoryTest extends DefaultConfigTest {
	
	@Autowired
	private ProvinceRepository provinceRepository;
	
	@Test
	public void shouldReturnRujaProvince() {
		Set<Province> provinces = provinceRepository.findByLocation(667, 556);
		assertThat(provinces, hasSize(1));
		assertThat(provinces, containsInAnyOrder(hasProperty("name", is("Ruja"))));
	}
	
	@Test
	public void shouldReturnNovaProvince() {
		Set<Province> provinces = provinceRepository.findByLocation(999, 333);
		assertThat(provinces, hasSize(1));
		assertThat(provinces, containsInAnyOrder(hasProperty("name", is("Nova"))));
	}
	
	@Test
	public void shouldReturnGodeAndRujaProvince() {
		Set<Province> provinces = provinceRepository.findByLocation(580, 939);
		assertThat(provinces, hasSize(2));
		assertThat(provinces,
			containsInAnyOrder(
				hasProperty("name", is("Ruja")),
				hasProperty("name", is("Gode"))));
	}
}
