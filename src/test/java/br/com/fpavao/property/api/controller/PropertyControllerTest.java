package br.com.fpavao.property.api.controller;

import br.com.fpavao.property.api.DefaultConfigTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Fabio E. Pavão
 * @since 1.0 - 11/06/17
 */
public class PropertyControllerTest extends DefaultConfigTest {
	
	@Autowired
	private MockMvc mvc;
	
	@Test
	public void shouldAddAProperty() throws Exception {
		
		StringBuilder newProperty = new StringBuilder("{ ");
		newProperty.append("\"id\": 1,");
		newProperty.append("\"title\": \"Imóvel código 1, com 3 quartos e 2 banheiros.\",");
		newProperty.append("\"price\": 643000,");
		newProperty.append("\"description\": \"Laboris quis quis elit commodo eiusmod qui exercitation. In laborum fugiat quis minim occaecat id.\",");
		newProperty.append("\"x\": 1257,");
		newProperty.append("\"y\": 928,");
		newProperty.append("\"beds\": 3,");
		newProperty.append("\"baths\": 2,");
		newProperty.append("\"squareMeters\": 61");
		newProperty.append(" }");
		
		mvc.perform(
				post("/properties")
					.accept(MediaType.APPLICATION_JSON_UTF8)
					.contentType(MediaType.APPLICATION_JSON)
					.content(newProperty.toString()))
			.andExpect(status().isCreated())
			.andExpect(header().string("location", containsString("/properties/1")))
			.andExpect(jsonPath("$.id", notNullValue()));
		;
	}
	
	@Test
	public void shouldReturnBadRequestBecauseBedField() throws Exception {
		
		StringBuilder newProperty = new StringBuilder("{ ");
		newProperty.append("\"id\": 2,");
		newProperty.append("\"title\": \"Imóvel código 2, com 4 quartos e 3 banheiros.\",");
		newProperty.append("\"price\": 949000,");
		newProperty.append("\"description\": \"Anim mollit aliqua adipisicing labore magna pariatur aute nulla. Amet veniam ut voluptate aliquip esse officia adipisicing ipsum.\",");
		newProperty.append("\"x\": 679,");
		newProperty.append("\"y\": 680,");
		newProperty.append("\"beds\": 6,");
		newProperty.append("\"baths\": 3,");
		newProperty.append("\"squareMeters\": 94");
		newProperty.append(" }");
		
		ResultActions result = mvc.perform(
			post("/properties")
				.accept(MediaType.APPLICATION_JSON_UTF8)
				.contentType(MediaType.APPLICATION_JSON)
				.content(newProperty.toString()))
			.andExpect(status().isBadRequest())
			.andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	public void shouldReturnBadRequestBecauseBathField() throws Exception {
		
		StringBuilder newProperty = new StringBuilder("{ ");
		newProperty.append("\"id\": 2,");
		newProperty.append("\"title\": \"Imóvel código 2, com 4 quartos e 3 banheiros.\",");
		newProperty.append("\"price\": 949000,");
		newProperty.append("\"description\": \"Anim mollit aliqua adipisicing labore magna pariatur aute nulla. Amet veniam ut voluptate aliquip esse officia adipisicing ipsum.\",");
		newProperty.append("\"x\": 679,");
		newProperty.append("\"y\": 680,");
		newProperty.append("\"beds\": 4,");
		newProperty.append("\"baths\": 5,");
		newProperty.append("\"squareMeters\": 94");
		newProperty.append(" }");
		
		mvc.perform(
			post("/properties")
				.accept(MediaType.APPLICATION_JSON_UTF8)
				.contentType(MediaType.APPLICATION_JSON)
				.content(newProperty.toString()))
			.andExpect(status().isBadRequest());
	}
	
	@Test
	public void shouldReturnBadRequestBecauseSquareMetersField() throws Exception {
		
		StringBuilder newProperty = new StringBuilder("{ ");
		newProperty.append("\"id\": 2,");
		newProperty.append("\"title\": \"Imóvel código 2, com 4 quartos e 3 banheiros.\",");
		newProperty.append("\"price\": 949000,");
		newProperty.append("\"description\": \"Anim mollit aliqua adipisicing labore magna pariatur aute nulla. Amet veniam ut voluptate aliquip esse officia adipisicing ipsum.\",");
		newProperty.append("\"x\": 679,");
		newProperty.append("\"y\": 680,");
		newProperty.append("\"beds\": 4,");
		newProperty.append("\"baths\": 3,");
		newProperty.append("\"squareMeters\": 241");
		newProperty.append(" }");
		
		mvc.perform(
			post("/properties")
				.accept(MediaType.APPLICATION_JSON_UTF8)
				.contentType(MediaType.APPLICATION_JSON)
				.content(newProperty.toString()))
			.andExpect(status().isBadRequest());
	}
	
}
